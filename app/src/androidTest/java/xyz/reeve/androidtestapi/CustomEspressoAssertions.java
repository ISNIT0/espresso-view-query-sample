package xyz.reeve.androidtestapi;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import junit.framework.AssertionFailedError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.Callable;

public class CustomEspressoAssertions {
    public static ViewAssertion hasChildrenCount(final int assertCount){
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException noViewFoundException) {
                if(view instanceof RecyclerView) {
                    RecyclerView recyclerView = (RecyclerView)view;
                    int count = recyclerView.getChildCount();
                    if(count != assertCount) {
                        throw new AssertionFailedError("Expected ["+assertCount+"] children in RecyclerView but got ["+count+"]");
                    }
                } else {
                    throw new AssertionFailedError("Could not find view of type [RecyclerView]");
                }
            }
        };
    }

    public static ViewAssertion childrenSortedBy(final Comparator comparator){
        return (view, noViewFoundException) -> {
            if(view instanceof RecyclerView) {
                RecyclerView recyclerView = (RecyclerView)view;
                int count = recyclerView.getChildCount();

                ArrayList<View> children = new ArrayList<>();

                for(int index = 0; index < count; ++index) {
                    View nextChild = recyclerView.getChildAt(index);

                    children.add(nextChild);
                }

                ArrayList tmp = new ArrayList(children);

                Collections.sort(tmp, comparator);

                if(!tmp.equals(children)) {
                    throw new AssertionFailedError("Expected recyclerView children to be:" +
                            "\n[" + tmp.subList(0, 5).toString() + "...]" +
                            "\nBut got:\n[" + children.subList(0, 5).toString() + "...]");
                }
            } else {
                throw new AssertionFailedError("Could not find view of type [RecyclerView]");
            }
        };
    }
}