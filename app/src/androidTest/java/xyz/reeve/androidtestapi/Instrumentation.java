package xyz.reeve.androidtestapi;

import android.support.constraint.ConstraintLayout;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.Callable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static xyz.reeve.androidtestapi.CustomEspressoAssertions.childrenSortedBy;
import static xyz.reeve.androidtestapi.CustomEspressoAssertions.hasChildrenCount;
import static xyz.reeve.androidtestapi.TestUtils.addAnimalToList;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class Instrumentation {
    Comparator<View> compareRowAtoZ = (o1, o2) -> {
        String o1s = ((TextView)o1.findViewById(R.id.tvAnimalName)).getText().toString();
        String o2s = ((TextView)o2.findViewById(R.id.tvAnimalName)).getText().toString();
        return o1s.toLowerCase().compareTo(o2s.toLowerCase());
    };

    Comparator<View> compareRowZtoA = (o1, o2) -> {
        String o1s = ((TextView)o1.findViewById(R.id.tvAnimalName)).getText().toString();
        String o2s = ((TextView)o2.findViewById(R.id.tvAnimalName)).getText().toString();
        return o2s.toLowerCase().compareTo(o1s.toLowerCase());
    };

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule(MainActivity.class);

    @Test
    public void simpleTest() {
        MainActivity mActivity = mActivityRule.getActivity();
        ConstraintLayout layout = (ConstraintLayout)mActivity.findViewById(R.id.activity_main_layout);

        RecyclerView list = layout.findViewById(R.id.recycler_list);
        int actualChildrenCount = list.getChildCount();

        onView(withId(R.id.recycler_list)).check(hasChildrenCount(actualChildrenCount));
    }

    @Test
    public void addAnimal(){
        MainActivity mActivity = mActivityRule.getActivity();
        ConstraintLayout layout = (ConstraintLayout)mActivity.findViewById(R.id.activity_main_layout);

        RecyclerView list = layout.findViewById(R.id.recycler_list);
        int actualChildrenCount = list.getChildCount();

        onView(withId(R.id.recycler_list)).check(hasChildrenCount(actualChildrenCount));

        addAnimalToList("Cougar");

        onView(withId(R.id.recycler_list)).check(hasChildrenCount(actualChildrenCount + 1));
    }

    @Test
    public void sortAtoZ(){

        onView(withId(R.id.atoz))
                .perform(click());

        onView(withId(R.id.recycler_list))
                .check(childrenSortedBy(compareRowAtoZ));
    }

    @Test
    public void sortZtoA(){

        onView(withId(R.id.ztoa))
                .perform(click());

        onView(withId(R.id.recycler_list))
                .check(childrenSortedBy(compareRowZtoA));
    }

    @Test
    public void sortAtoZAndAddItem(){
        sortAtoZ();

        addAnimal();

        onView(withId(R.id.recycler_list))
                .check(childrenSortedBy(compareRowAtoZ));
    }
}