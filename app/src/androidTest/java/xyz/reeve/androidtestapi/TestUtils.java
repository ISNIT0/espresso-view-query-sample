package xyz.reeve.androidtestapi;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class TestUtils {
    public static void addAnimalToList(String animalName){
        onView(withId(R.id.animalName))
                .perform(replaceText(animalName));
        onView(withId(R.id.addAnimal))
                .perform(click());

    }
}
