package xyz.reeve.androidtestapi;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
    MyRecyclerViewAdapter adapter;
    ArrayList<String> itemsToDisplay = new ArrayList<>();
    ArrayList<String> animalNames = new ArrayList<>();

    String orderBy = "atoz"; // "atoz", "ztoa", "length"
    String filter = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText animalInput = ((EditText)findViewById(R.id.animalName));
        final EditText filterInput = findViewById(R.id.filter);

        Button addAnimalButton = findViewById(R.id.addAnimal);

        animalNames.add("Horse");
        animalNames.add("Cow");
        animalNames.add("Camel");
        animalNames.add("Sheep");
        animalNames.add("Goat");

        filterInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter = filterInput.getText().toString();
                updateItemsToDisplay();
            }
        });

        // set up the RecyclerView
        final RecyclerView recyclerView = findViewById(R.id.recycler_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, itemsToDisplay);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        updateItemsToDisplay();


        findViewById(R.id.atoz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderBy = "atoz";
                orderList();
            }
        });

        findViewById(R.id.ztoa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderBy = "ztoa";
                orderList();
            }
        });

        findViewById(R.id.length).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderBy = "length";
                orderList();
            }
        });

        addAnimalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String animal = animalInput.getText().toString();
                if(animal.length() > 0) {
                    animalNames.add(animal);
                    //orderList(); //Deliberately cause a "bug" when list is already sorted
                    animalInput.setText("");
                    updateItemsToDisplay();
                }
            }
        });
    }

    private void orderList(){
        Collections.sort(animalNames, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(orderBy == "atoz") {
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                } else if(orderBy == "ztoa") {
                    return o2.toLowerCase().compareTo(o1.toLowerCase());
                } else { //length
                    return o1.length() - o2.length();
                }
            }
        });
        updateItemsToDisplay();
    }

    private void updateItemsToDisplay(){
        itemsToDisplay.clear();
        for (int index = 0; index < animalNames.size(); ++index) {
            String item = animalNames.get(index);
            if (filter.length() == 0 || item.toLowerCase().contains(filter.toLowerCase())) {
                itemsToDisplay.add(item);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }
}
